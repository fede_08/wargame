"""
Test cases
"""
from django.test import TestCase

# Create your tests here.
from Game.models import UnitWithWeapon, Unit, Army, Weapon


class Test(TestCase):

    def setUp(self) -> None:
        """
        Create units, army and weapons for test
        """
        self.knight = UnitWithWeapon(100, 20)
        self.archer = UnitWithWeapon(50, 25)
        self.catapult = Unit(200, 50)
        self.first_army = Army()
        self.second_army = Army()
        self.sword = Weapon(10)
        self.shield = Weapon(0)
        self.silver_arrow = Weapon(4)

    def test_unit_get_life_points(self):
        self.assertEqual(self.knight.get_life(), 100)
        self.assertEqual(self.archer.get_life(), 50)
        self.assertEqual(self.catapult.get_life(), 200)

    def test_unit_get_attack_points(self):
        self.assertEqual(self.knight.get_damage(), 20)
        self.assertEqual(self.archer.get_damage(), 25)
        self.assertEqual(self.catapult.get_damage(), 50)

    def test_add_unit_to_army(self):
        self.first_army.add_unit(self.knight)
        self.first_army.add_unit(self.catapult)
        self.first_army.add_unit(self.archer)
        army_list = [self.knight, self.catapult, self.archer]
        self.assertEqual(self.first_army.group, army_list)

    def test_add_army_to_army(self):
        self.first_army.add_unit(self.knight)
        self.first_army.add_unit(self.catapult)
        self.first_army.add_unit(self.archer)
        self.second_army.add_unit(self.archer)
        self.second_army.add_unit(self.first_army)
        second_army_list = [self.archer, self.first_army]
        self.assertEqual(self.second_army.group, second_army_list)

    def test_remove_unit_to_army(self):
        self.first_army.add_unit(self.knight)
        self.first_army.add_unit(self.catapult)
        self.first_army.add_unit(self.archer)
        self.first_army.remove_unit(self.catapult)
        army_list = [self.knight, self.archer]
        self.assertEqual(self.first_army.group, army_list)

    def test_remove_army_to_army(self):
        self.first_army.add_unit(self.knight)
        self.second_army.add_unit(self.archer)
        self.second_army.add_unit(self.first_army)
        self.second_army.remove_unit(self.first_army)
        army_list = [self.archer]
        self.assertEqual(self.second_army.group, army_list)

    def test_add_weapon_to_unit(self):
        self.knight.add_weapon(self.sword)
        self.knight.add_weapon(self.shield)
        weapon_list = [self.sword, self.shield]
        self.assertEqual(self.knight.equipment, weapon_list)
        self.assertEqual(self.knight.get_damage(), 30)

    def test_remove_weapon_to_unit(self):
        self.knight.add_weapon(self.sword)
        self.knight.remove_weapon(self.sword)
        weapon_list = []
        self.assertEqual(self.knight.equipment, weapon_list)

    def test_is_army(self):
        self.assertTrue(self.first_army.is_army())
        self.assertFalse(self.catapult.is_army())

    def test_unit_attack_unit(self):
        self.knight.attack(self.catapult)
        self.assertEqual(self.catapult.get_life(), 180)
        self.archer.add_weapon(self.silver_arrow)
        self.archer.attack(self.catapult)
        self.assertEqual(self.catapult.get_life(), 151)

    def test_unit_cant_attack(self):
        self.catapult.attack(self.knight)
        self.knight.attack(self.catapult)
        self.assertEqual(self.catapult.get_life(), 180)
        self.catapult.attack(self.knight)
        self.knight.attack(self.catapult)
        self.assertEqual(self.catapult.get_life(), 180)

    def test_unit_cant_attack_army(self):
        try:
            self.catapult.attack(self.first_army)
        except Exception as e:
            print(e)
        try:
            self.second_army.attack(self.first_army)
        except Exception as e:
            print(e)

    def test_army_attack_unit(self):
        self.first_army.add_unit(self.knight)
        self.first_army.add_unit(self.catapult)
        self.first_army.attack(self.catapult)
        self.assertEqual(self.catapult.get_life(), 130)
