"""
Inkan Exercise
"""
import abc
from typing import List

# Create your models here.
from django.db import models


class Weapon(models.Model):
    """
    Weapon for army units
    """
    damage = models.IntegerField()

    def __init__(self, value: int, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.damage = value


class Troop(abc.ABC):
    """
    The base Toop class declares common operations for both simple and
    complex army units.
    """

    def is_army(self) -> bool:
        """
        Indicates if it is a complex army unit
        """
        return False

    @abc.abstractmethod
    def attack(self, unit) -> None:
        """
        Attack another unit
        """
        pass

    @abc.abstractmethod
    def get_damage(self) -> int:
        """
        Get army unit damage
        """
        pass

    @abc.abstractmethod
    def get_life(self) -> int:
        """
        Get army unit damage
        """
        pass

    def remove_life(self, param):
        """
        Reduce life points
        """
        pass

    def is_defeat(self) -> bool:
        """
        Troop is defeat
        """
        return not self.get_life() > 0


class Unit(Troop):
    """
    Unit class represents a simple army units
    """
    life = models.IntegerField()
    damage = models.IntegerField()

    def __init__(self, life_point: int, damage_point: int):
        self.life = life_point
        self.damage = damage_point

    def remove_life(self, damage: int) -> None:
        """
        Remove life from unit
        """
        self.life -= damage

    def attack(self, unit: Troop) -> None:
        """
        Attack another army unit if self has life grater than zero and the army unit isn't an Army and its life is
        grater than zero.
        """
        if unit.is_army():
            raise Exception("Sorry, you can't attack the Army")
        if not self.is_defeat() and not unit.is_defeat():
            unit.remove_life(self.get_damage())

    def get_life(self) -> int:
        """
        Get unit life
        """
        return self.life

    def get_damage(self) -> int:
        """
        Get unit damage
        """
        return self.damage


class UnitWithWeapon(Unit):
    """
    This is a special class of army unit that can carry equipment for increase its attack
    """

    def __init__(self, life_point: int, damage_point: int):
        super().__init__(life_point, damage_point)
        self._equipment: List[Weapon] = []

    @property
    def equipment(self) -> List:
        """
        Returns a list of equipped weapons
        """
        return self._equipment

    def add_weapon(self, weapon: Weapon) -> None:
        """
        Add weapon to the unit equipment
        """
        self._equipment.append(weapon)

    def remove_weapon(self, weapon: Weapon) -> None:
        """
        Remove weapon from the unit equipment
        """
        try:
            self._equipment.remove(weapon)
        except ValueError:
            pass

    def get_damage(self) -> int:
        """
        Get unit damage
        """
        total_attack = self.damage
        for weapon in self._equipment:
            total_attack += weapon.damage
        return total_attack


class Army(Troop):
    """
    Army class represents a complex army units
    """

    def __init__(self):
        self._group: List[Troop] = []

    @property
    def group(self) -> List:
        """
        Returns a list of army troops
        """
        return self._group

    def get_life(self) -> int:
        """
        Get Army life
        """
        total_life = 0
        for unit in self._group:
            if unit.get_life() > 0:
                total_life += unit.get_life()
        return total_life

    def get_damage(self) -> int:
        """
        Get Army damage
        """
        total_attack = 0
        for unit in self._group:
            if unit.get_life() > 0:
                total_attack += unit.get_damage()
        return total_attack

    def attack(self, unit: Troop) -> None:
        """
        Attack another army unit if self has life grater than zero and the army unit isn't an Army and its life is
        grater than zero.
        """
        if unit.is_army():
            raise Exception("Sorry, you can't attack the Army")
        elif self.get_life() > 0 and unit.get_life() > 0:
            unit.remove_life(self.get_damage())

    def add_unit(self, unit: Troop) -> None:
        """
        Add army unit to the Army
        """
        self._group.append(unit)

    def remove_unit(self, unit: Troop) -> None:
        """
        Remove army unit to the Army
        """
        try:
            self._group.remove(unit)
        except ValueError:
            pass

    def is_army(self) -> bool:
        """
        Indicates if it is a complex army unit
        """
        return True
